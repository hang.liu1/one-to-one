package com.twuc.webApp;

import com.twuc.webApp.domain.Account;
import com.twuc.webApp.domain.AccountRepository;
import com.twuc.webApp.domain.Profile;
import com.twuc.webApp.domain.ProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;


@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class OneOnOneTest {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private EntityManager entityManager;

    private void flushAndClear(Runnable run){
        run.run();
        entityManager.flush();
        entityManager.clear();
    }

    @Test
    void should_() {
        flushAndClear(() -> {
            Account account = new Account();

            Profile profile = new Profile();

            profile.setAccount(account);
            account.setProfile(profile);

            accountRepository.save(account);
        });


    }
}
